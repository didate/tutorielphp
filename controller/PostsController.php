<?php
class PostsController extends Controller {
	
	function index(){
		$perPage =2;
		$this->loadModel('Post');
		$conditions = array('type'=>'post','online'=>1);
		$d['posts'] = $this->Post->find(array(
			'conditions' => $conditions,
			'limit'=> ($perPage * ($this->request->page-1)).','.$perPage
		));
		$d['total'] = $this->Post->findCount($conditions);
		$d['page'] = ceil($d['total']/$perPage);
		$this->set($d);
	}


	function view($id){
		$this->loadModel('Post');
		$conditions = array('id'=>$id,'type'=>'post','online'=>1);
		$d['post'] = $this->Post->findFirst(array(
			'conditions' => $conditions
		));

		
		if(empty($d['post'])){
			$this->e404('Page introuvable');
		}

		$this->set($d);
	}


}

?>

