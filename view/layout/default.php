<html>
<head>
	<title><?php echo isset($title_for_layout)? $title_for_layout : "Mon Site" ?></title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-inverse navbar-static-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Mon blog</a>
	    </div>
	    <div>
	      <ul class="nav navbar-nav">
	      	<?php $pagesMenu = $this->request('Pages','getMenu'); ?>
	      	<?php
	      		foreach ($pagesMenu as $page) {
	      	?>
	      			<li>
	      				<a href="<?php echo BASE_URL.'/pages/view/'.$page->id; ?>">
	      					<?php echo $page->name; ?>
	      				</a>
	      		</li>
	      	<?php
	      		}
	      	?>
	      	<li> <a href="<?php echo BASE_URL.'/posts' ?>">Actualités</a> </li>
	        
	       </ul>
	    </div>
	  </div>
	</nav>
	<div class="container">
		<?php echo $content_for_layout; ?>
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>